var app = new Vue({
    el : "#app",
    data : {
        message : "HELLO WORLD",
        isActive : true,
        activeClass : "active",
        textClass : "text",

        activeColor: 'red',
        fontSize: 30,

        styleObject: {
            color: 'blue',
            fontSize: '30px',
            marginTop: '10px',
        }
    }
});