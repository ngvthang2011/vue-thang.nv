var app = new Vue({
    el : '#app',
    data : {
        category : {
            id : 1,
            name : "Điện thoại"
        },
        message : 'hello World',
        isInputDisabled : false
    },
    methods: {
        handChangeTitle() {
            // sử dụng v-one data chỉ nạp 1 lần vào dom và không bị thay đổi
            this.message = this.message+' 123456';
        },
        handleChangeStatusInput() {
            this.isInputDisabled = ! this.isInputDisabled;
        }
    },
});