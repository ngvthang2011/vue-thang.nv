import ComponentHeader from './components/compHeader'

var app = new Vue({
    el : "#app",
    components : {
        'comp-header' : ComponentHeader
    },
    data : {
        message : "HELLO"
    }
});