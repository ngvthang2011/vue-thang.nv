export default {
    props : {
        message
    },
    template : `
        <div class="header">
            <h1>{{ message }}</h1>
        </div>
    `
}