Vue.component('comp-body', {
    props : {
        title : {
            type : String,
            default : "DEFAULT"
        }
    },
    template : `
        <div class="content">
            <h3>{{ title }}</h3>
            <slot></slot>
            <h3>END</h3>
            <slot name="footer"></slot>
        </div>
    `
});
var app = new Vue({
    el : "#app",
    data : {
        title : "Pháp luật"
    }
});