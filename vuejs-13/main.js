Vue.component('comp-header', {
    props : {
        title : {
            type : String,
            default : 'Hello World'
        }
    },
    template : `
    <div>
        <h1>{{title}}</h1>
    </div>
    `
});

var app = new Vue({
    el : "#app",
    data : {
        titleHeader : "TITLE HEADER"
    }
});