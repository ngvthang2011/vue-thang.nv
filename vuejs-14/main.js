Vue.component('comp-header', {
    props : {
        title : {
            type : String,
            default : "Hello World"
        }
    },
    template : `
        <div class="header">
            <h1>{{ title }}</h1>
            <button @click="handleChangeTitle">Change Title</button>
        </div>
    `,
    methods: {
        handleChangeTitle() {
            console.log('handleChangeTitle');
            this.$emit('change-title');
        }
    },
});

Vue.component('base-checkbox', {
    model: {
      prop: 'checked',
      event: 'change'
    },
    props: {
      checked: Boolean
    },
    template: `
      <input
        type="checkbox"
        v-bind:checked="checked"
        v-on:change="$emit('change', $event.target.checked)"
      >
    `
  })

Vue.component('comp-footer', {
    props : {
        title : {
            type : String,
            default : "Hello World"
        }
    },
    template : `
        <div class="footer">
            <h1>{{ title }}</h1>
        </div>
    `
});

var app = new Vue({
    el : "#app",
    data : {
        title : "TITLE HEADER AND FOOTER",
        lovingVue : true
    },
    methods: {
        changeTitle() {
            console.log('changeTitle');
            this.title = "Title changed succesfuly"
        }
    },
});