var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello word '
    }
})

var app1 = new Vue({
    el: '#app1',
    data: {
      title: 'Loaded page on ' + new Date().toLocaleString()
    }
})

var app2 = new Vue({
    el: '#app2',
    data: {
      seen: false
    }
})

var app3 = new Vue({
    el: '#app3',
    data: {
      todos: [
        { text: 'Learn JavaScript' },
        { text: 'Learn Vue' },
        { text: 'Learn Laravel' }
      ]
    }
})

var app4 = new Vue({
    el: '#app4',
    data: {
      message: 'Hello Vue.js!',
      statusMessage : true
    },
    methods: {
        changeMessage() {
            this.message = this.message.split('').reverse().join('.');
        },
        handleHiddenShow() {
            this.statusMessage = ! this.statusMessage;
        }
    }
})

var app5 = new Vue({
    el: '#app5',
    data: {
      message: 'Hello Vue!'
    }
})

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

var app6 = new Vue({
    el: '#app6',
    data: {
      categoryList: [
        { id: 0, text: 'Iphone' },
        { id: 1, text: 'SamSung' },
        { id: 2, text: 'OPPO' },
        { id: 3, text: 'HTC' }
      ]
    }
})
