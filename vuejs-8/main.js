var app = new Vue({
    el : "#app",
    data : {
        listProduct : [
            {id: 1, name: 'Iphone 1', categoryId: 1, isStock: true},
            {id: 2, name: 'Iphone 2', categoryId: 1, isStock: true},
            {id: 3, name: 'SamSung 3', categoryId: 2, isStock: false},
            {id: 4, name: 'SamSung 4', categoryId: 2, isStock: true},
        ],
        product : {
            id : 20,
            name : 'Iphone XS Max 256GB',
            price : 10000
        }
    }
});