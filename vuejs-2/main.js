// var obj = {
//     foo: 'bar'
//   }
  
//Object.freeze(obj) //freeze không cho thay đổi
  

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

new Vue({
    el: '#app',
    data: {
        obj : {
            foo: 'bar'
        },
        categoryList: [
            { id: 0, text: 'Iphone' },
            { id: 1, text: 'SamSung' },
            { id: 2, text: 'OPPO' },
            { id: 3, text: 'HTC' }
          ]
    },
    methods: {
        
    },
    beforeCreate() {
        console.log('beforeCreate', this.categoryList, document.querySelector('#todo-list'));
    },
    created() {
        // thường gọi API
        console.log('created', this.categoryList, document.querySelector('#todo-list'));
    },
    // khi Vue instant có sự update data sẽ chạy tuần tự 2 hàm dưới
    beforeUpdate() {
        // chạy khi data vừa thay đổi và chưa render ra DOM
    },
    updated() {
        // sẽ chạy khi đã render xong ở DOM
    },
    beforeMount() {
        console.log('beforeMount', this.categoryList, document.querySelector('#todo-list'));
    },
    mounted() {
        // chạy khi insatant được render ra, thường init library, setup gì đó cho component
        console.log('mounted', this.categoryList, document.querySelector('#todo-list'));
    },
    beforeDestroy() {
        //chạy khi trước khi component bị xóa
        console.log('beforeDestroy', this.categoryList, document.querySelector('#todo-list'));
    },
    destroyed() {
        //chạy khi component đã bị xóa toàn bộ bao gồm cả data, dom, events,…
        console.log('destroyed', this.categoryList, document.querySelector('#todo-list'));
    },
})