// computed không cho phép truyền tham số còn method với watch(newValue, oldValue) thì có truyền được.
// computed thực hiện khi có thành phần phụ thuộc thay đổi, nên sử dụng khi thao tác với dữ liệu trong instant mà hạn chế tính toán lại
// method thực hiện khi có bất cứ thay đổi nào(re-render), sử dụng khi cần 1 hàm bình thường hoặc có truyền tham số
// watch thực hiện khi thuộc tính, đối tượng đã khai báo có sự thay đổi, sử dụng khi dữ liệu thay đổi nhiều liên tục(ví dụ sử lý thanh tìm kiếm)
var app = new Vue({
    el : "#app",
    data : {
        a : 0,
        b : 0,
        number : 0,
        message : 'Hello World',
        firstName : 'Thắng',
        lastName : 'Nguyễn',
        question: '',
        answer: 'I cannot give you an answer until you ask a question!'
    },
    watch : {
        question: function (newQuestion, oldQuestion) {
            this.answer = 'Waiting for you to stop typing...'
        }
    },
    computed : {
        reversedMessage: function () {
            return this.message.split('').reverse().join('')
        },
        fullName: {
            // getter
            get: function () {
              return this.firstName + ' ' + this.lastName
            },
            // setter
            set: function (newValue) {
              var names = newValue.split(' ')
              this.firstName = names[0]
              this.lastName = names[names.length - 1]
            }
        }
    },
    methods: {
        addA() {
            console.log('add A');
            return this.a;
        },
        addB() {
            console.log('add B');
            return this.b;
        }
    },
});